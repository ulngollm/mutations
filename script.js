const config = {
  childList: true, // наблюдать за непосредственными детьми
  subtree: true, // и более глубокими потомками
  attributes: true,
  characterDataOldValue: true, // передавать старое значение в колбэк
  attributeFilter: ['style', 'data-bla' ]
};
const callback = function (mutationRecords) {
  console.log(mutationRecords);
};
const target = document.querySelector(".target");
const observer = new MutationObserver(callback);
// Начинаем наблюдение за настроенными изменениями целевого элемента
observer.observe(target, config);

// Позже можно остановить наблюдение
// observer.disconnect();
target.insertAdjacentHTML('afterend', '<asd></asd>')

